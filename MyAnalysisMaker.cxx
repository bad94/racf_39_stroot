/***********************************************************************
* Author      : Niseem ABDELRAHMAN                                     *
* Description : A simple analysis Maker.                               *
*               Some modification to my StMyMaker.                     *
* Data        : auau_39_2010                                           *
* Start Date  :   / /2014                                              *
* End Date    :  / /2014                                               *
* Version     : 0.0.0( vn_PID Analysis )                               *
***********************************************************************/
#include "StMuDSTMaker/COMMON/StMuDstMaker.h"
#include "StMuDSTMaker/COMMON/StMuTrack.h"
#include "StMuDSTMaker/COMMON/StMuEvent.h"
#include "StRoot/StRefMultCorr/StRefMultCorr.h"
#include "HBTmixcheck.h"
#include "HBTmixcheck.C"
#include "MyAnalysisMaker.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TProfile.h"
#include "TProfile2D.h"
#include "TProfile3D.h"
#include "TMath.h"
#include "TFile.h"
#include "TObjArray.h"
#include "TList.h"
#include "TString.h"
#include "TRandom.h"                            // Used in PID selection
#include "TRandom3.h"                            // Used in PID selection
#include "EventPlane.C"
//============================================

#define NumberOfTH1F       50    // Number of Histograms (must be less than max in .h file)
#define NumberOfTH2F       50    // Number of Histograms (must be less than max in .h file)
#define NumberOfTH2D       50    // Number of Histograms (must be less than max in .h file)
#define NumberOfTH3F       150   // Number of Histograms (must be less than max in .h file)
#define NumberOfTProfile   150   // Number of Histograms (must be lesss than max in .h file) 
#define NumberOfTProfile2D 150  // Number of Histograms (must be less than max in .h file)        
#define MinTracks          4    // Discard events with fewer than MinTracks (Flow with 2 tracks makes no sense :-)
#define MinEvents          3    // Discard DST data when we have fewer than MinEvents on a file sequence
#define NMAX               2000   
//============================================
//hbt_sgl_trk*  DataArray[10][10][20][2000][10];
//const char MyData[NMAX]="StRoot/MyAnalysis/MyData/Radi_27Gev_%d.txt";
//const char MyQinvData[NMAX]="StRoot/MyAnalysis/MyData/Qinv_27Gev_%d.txt";
float  DataArray1[2000][2];
float  nFlowTracks1[10][10][20][1];
int    ran_map[10][10][20][2000];
int    ran_map1[NMAX];
float  pi = 3.14159265;
char   histname[NMAX];
char   DataFileName[NMAX];
char   histname22[NMAX];
//============================================
ClassImp(MyAnalysisMaker)                       // Macro for CINT compatibility
//============================================
MyAnalysisMaker::MyAnalysisMaker( StMuDstMaker* maker ) : StMaker("MyAnalysisMaker")
{ 
  //Initialize and/or zero all public/private data members here.    
  for(Int_t i=0; i<10; i++)
    {
      for(Int_t j=0; j<10; j++)
	{
	  for(Int_t k=0; k<20; k++)
	    {
	      for(Int_t m=0; m<1; m++)
		{
		  nFlowTracks1[i][j][k][m] = 0.0;
		}
	    }
	}
    }	
  //=========================	      
  for(Int_t i=0; i<NumberOfTH1F; i++)
    {
      histogram[i] = NULL;
    }
  //=========================	      
  for ( Int_t i = 0 ; i < NumberOfTH2F ; i++ )  // Zero the 2D histogram pointers    
    {
      histogram2D[i] = NULL;
    }
  //=========================	      
  for(Int_t i=0; i<10; i++)
    {
      for(Int_t k=0; k<20; k++)
	    {
	      mEventsProcessed[i][k]  =  0    ;// Zero the Number of Events processed by the maker  
	    }
    }
  mEventsProcessed1 =   0    ;// Zero the Number of Events processed by the maker  
  //==========================
  histogram_output  =  NULL  ; // Zero the pointer to histogram output file
  mMuDstMaker       =  maker ; // Pass MuDst pointer to AnlysisMaker class member functions
  mEventsStarted    =  0     ;      
  mHistogramOutputFileName = "";// Histogram Output File Name will be set inside the .C macro
  

}
//============================
MyAnalysisMaker::~MyAnalysisMaker() 
{ 
  // Destroy and/or zero out all public/private data members here.
}
//============================
//============================
Int_t MyAnalysisMaker::Init( )
{ 
// Do once at the start of every analysis
  refmultCorrUtil=new StRefMultCorr();
  rand = new TRandom() ;   
  histogram_output = new TFile( mHistogramOutputFileName, "recreate" ) ;  // Name was set in "analysis".C macro
  
  /*OLD HISTOGRAMS
  histogram[0]     = new TH1F ( "RefMult", "RefMult", 10, 0.0, 10.0 ) ;
  histogram[1]     = new TH1F ( "Mult", "Mult", 1000, 0.0, 1000.0 ) ;
  histogram[2]     = new TH1F ( "Vx", "Vx", 10, -1.0, 1.0) ;
  histogram[3]     = new TH1F ( "Vy", "Vy", 10, -1.0, 1.0) ;
  histogram[4]     = new TH1F ( "Vz", "Vz", 20, -50.0, 50.0) ;
  histogram2D[0]   = new TH2F ( "NPrimary_NGlobal_Pure", "NPrimary_NGlobal_Pure", 1500, 0, 1500, 1500, 0, 1500 ) ;
  histogram2D[1]   = new TH2F ( "NPrimary_vs_NGlobal_AfterEventCuts", "NPrimary_vs_NGlobal_AfterEventCuts", 1500, 0, 1500, 1500, 0, 1500 ) ;
  histogram2D[2]   = new TH2F ( "P_dEdx_pi"  , "P_dEdx_pi"  , 100, -4., 4., 100, 0.0, 0.00001 ) ;
  histogram2D[3]   = new TH2F ( "P_ivbeta_pi", "P_ivbeta_pi", 100, -4., 4., 200, 0.0, 5.0 ) ;
  histogram2D[4]   = new TH2F ( "P_dEdx"     , "P_dEdx"    , 100, -4., 4., 100, 0.0, 0.00001 ) ;
  */
  //===========================////////////////////////////////////////// NEW HISTOGRAMS BELOW-------------------------------------------  
  Zcut = 40;
  histogram[0]      = new TH1F("multip","multip",1000,1,1000);
  histogram[3]      = new TH1F("multip_cuts","multip_cuts",1000,1,1000);
  histogram[1]      = new TH1F("pt","pt",100,0,2);
  histogram[2]      = new TH1F("cent","cent",10,0,9);  
  histogram2D[0]    = new TH2F ( "p_dEdX"  , "dEdX"  , 100, -4., 4., 100, 0.0, 0.00001 ) ;
  histogram2D[1]    = new TH2F ( "p_dEdX_pi"  , "p_dEdX_pi"  , 100, -4., 4., 100, 0.0, 0.00001 ) ;
  histogram[4]      = new TH1F ("zvertex_cuts","zvertex_cuts",100,-50,50);

  hbt = new HBTmixcheck();
  Cent_hbt=(int)hbt->Ncent_mix;
  //qvector=true{analysis will output qvector files only}, qvector=false{analysis assumes qvector files are in place}
  //makeweightfiles=true{analysis qill ouput detector acceptance etaphi files only} makeweightfiles=false{analysis assumes eta phi acceptance files are in place}
  bool qvector = false;
  bool makeweightfiles =false;
  if(qvector==false && makeweightfiles==false){cout<<"weights good...initialize hbt"<<endl; hbt->Init();}
  EP = new EventPlane(Cent_hbt,hbt->Nrp_mix,Zcut,qvector,makeweightfiles); 
  cout<<"Done with initializingggg"<<endl;
  return kStOK;
}
//=========================	      
Int_t MyAnalysisMaker::Make( )
{
  //  int        nFlowTracks = 0;
  StMuDst*   mu      =  mMuDstMaker->muDst(); if (!mu)      return kStOK ;
  StMuEvent* muEvent =  mu->event()         ; if (!muEvent) return kStOK ;
  
  //===========================
  mEventsStarted++ ;
  //===========================
  histogram2D[0]->Fill((float)mMuDstMaker->muDst()->globalTracks()->GetEntries(), (float)mMuDstMaker->muDst()->primaryTracks()->GetEntries());
  //===========================
  if(!accepttrigger(muEvent))       return kStOK;                  // Skip this event if it doens't pass the trigger cuts 
  if(!RejectRunNumbers(muEvent))    return kStOK;
  if(!acceptevent(muEvent))         return kStOK;
  histogram2D[1]->Fill((float)mMuDstMaker->muDst()->globalTracks()->GetEntries(), (float)mMuDstMaker->muDst()->primaryTracks()->GetEntries());

  //===========================
  refmultCorrUtil->init(muEvent->runNumber());
  refmultCorrUtil->initEvent(muEvent->refMult(), muEvent->primaryVertexPosition().z(), muEvent->runInfo().zdcCoincidenceRate());

  //===========================
  const Int_t  Multiplicity  =  refmultCorrUtil->getCentralityBin9();
  Int_t        CentralityID  =  centrality(Multiplicity);
  TObjArray*     tracks = mMuDstMaker->muDst()->primaryTracks(); // Create a TObj array containing the primary tracks   
  TObjArrayIter  GetTracks(tracks) ; // Create an iterator to step through the tracks             
  //TList          PionList; // TList to store all particles in one event for later loop & recall       
  StMuTrack*     track;
    histogram[0]->Fill(Multiplicity);
    //===========================Event Level Cuts===========================================
    int hbt_centrality_array[10]={9,8,7,6,5,4,3,2,1,0};
    CentralityID = hbt_centrality_array[CentralityID];
    ClassID cls_id;//hbt class
    event_trk tmp_event; //event_trk data members
    if(CentralityID>=((int)hbt->Ncent_mix ) || CentralityID>=( (int)hbt->Ncent )) return kStOK;
    cls_id.cnt_class = CentralityID;
    cls_id.rp_class  = 0;//initialize
    tmp_event.Event_Cent_Class =CentralityID;
    histogram[2]->Fill(CentralityID);
    //VERTEX CUT
    int    trackzx=-999;
    int Vz = muEvent->primaryVertexPosition().z();
    if(fabs(Vz)>Zcut) return kStOK;
    for(int i=-Zcut; i<= Zcut; i+=5){
      if(Vz>i && Vz<=(i+5) ){trackzx = (i+Zcut)/5;}
            if(trackzx> (int)(hbt->Nzvertex_mix) ){cout<<"nvertexz class fail, change in .h file"<<endl; return kStOK; }
    }
    if(trackzx ==-999) return kStOK;
    tmp_event.Event_ZVert_Class = trackzx;
    histogram[3]->Fill(Multiplicity);
    histogram[4]->Fill(Vz);

  while ( ( track = (StMuTrack*)GetTracks.Next() ) )// Main loop for Iterating over tracks
    {
      if (!accepttrack(track) ) continue; // Skip this track if it doesn't pass the 'track' cut
      /*
	float P  ;
	float pt;
	float eta;
	float phi;
	float beta;
	float DCA;
	float DeDx;
	float nSigmaPion;
	float nSigmaKaon;
	float nSigmaProton;
	float nSigmaElectron;
      */
      BenTrack Tempben;
      Tempben.P=track->p().mag();
      Tempben.beta=track->btofPidTraits().beta();
      Tempben.nSigmaPion=track->nSigmaPion();//nsigma  
      Tempben.nSigmaKaon=track->nSigmaKaon();//nsigma 
      Tempben.nSigmaElectron=track->nSigmaElectron();//nsigma 
      Tempben.nSigmaProton=track->nSigmaProton();
      Tempben.DeDx=track->dEdx();  
      Tempben.eta=track->eta(); 
      Tempben.phi=track->phi(); 
      Tempben.DCA=track->dcaGlobal().mag();
      Tempben.pt =track->pt();
      histogram[1]->Fill(fabs(Tempben.pt));
      hbt_sgl_trk tmp_trk;//hbt track	
      hbt->ResetHbtSglTrk( &tmp_trk );	
      int charge = track->charge();
      Tempben.pt=fabs(Tempben.pt);
      histogram2D[1]->Fill(fabs(Tempben.P),fabs(Tempben.DeDx) );
      EP->SumQVector(Tempben,charge,CentralityID,tmp_event.Event_ZVert_Class);
      //int ett;
      //if(Tempben.eta<0) ett=0;
      //if(Tempben.eta>0) ett=1;
      //if(ett!=1) continue;
      
      if((fabs(Tempben.nSigmaPion) < 2.0) && (fabs(Tempben.nSigmaKaon) > 2.0) && (fabs( Tempben.nSigmaElectron) > 2.0) && fabs(Tempben.P) < 0.6)// && Tempben.DeDx<3*pow(10,-6) )   //Pions
      {
       if(Tempben.pt>0.2){
         histogram2D[0]->Fill(Tempben.P,Tempben.DeDx);
         tmp_trk.particle_id = 0;
         hbt->SetTrksData(Tempben,tmp_trk );
         if( charge > 0 ){
	      tmp_event.sgl_trks[0].push_back( tmp_trk );//positive
	    }else if( charge < 0 ){
	      tmp_event.sgl_trks[1].push_back( tmp_trk );//negative
	    }else cout<<"error in charge"<<endl; //charge
	  }//pt cut
	}//pions
      
      //      if ( track->charge() < 0 ) continue;
     
      //==========================
      //trackP          =track->p().mag();
      //double trackBeta=track->btofPidTraits().beta();
      //trackPion       =track->nSigmaPion();//nsigma
      //trackKaon       =track->nSigmaKaon();//nsigma
      //trackElectron   =track->nSigmaElectron();//nsigma
      //trackdEdx       =track->dEdx();
      //trackEta        = track->eta();
      //trackPhi        = track->phi();
      //theta           = 2.0*atan(exp(-trackEta));
      //trackpx         = trackP*cos(trackPhi)*sin(theta);
      // trackpy        = trackP*sin(trackPhi)*sin(theta);
      //trackpz         = trackP*cos(theta);
      //=============================================
      //float constant_eff[10]={0.712, 0.761, 0.807, 0.871, 0.915, 0.929, 0.924, 0.92, 0.92, 0.92};//this pt independent part doesn't actually
      //float eff_pt = 1.0;
      //if(fabs(track->eta()) > 2.5) eff_pt = (1.4-0.2*fabs(track->eta()))*(1./(1.+exp(-(fabs(track->eta())-2.65)/0.1)))*(1./(1.+exp((fabs(track->eta())-3.9)/0.15)));
      //else eff_pt = constant_eff[CentralityID]/(1.+exp(-(track->pt()+0.10)/0.15));
      //==========================
      // if(trackBeta  == -999){
            /*
	else{
	//==========================
	histogram2D[12]->Fill(trackP,1/trackBeta);
	//==========================
	if(PionCheck(trackPion,trackBeta,trackP)) {
	double tempE = sqrt( pow(trackP, 2.0 ) + pow( 0.13957, 2.0 ) );
	if( fabs( 0.5 * log( (tempE+trackpz )/(tempE-trackpz) ) )>0.5) continue;
	DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][nFlowTracks][0]  = trackpx;
	DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][nFlowTracks][1]  = trackpy;
	DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][nFlowTracks][2]  = trackpz;
	DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][nFlowTracks][3]  = sqrt( pow(trackP, 2.0 ) + pow( 0.13957, 2.0 ) );       
	DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][nFlowTracks][4]  = 1/eff_pt ;
	DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][nFlowTracks][5]  = trackEta;
	DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][nFlowTracks][6]  = track->pt();
	DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][nFlowTracks][7]  = Phi;
	DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][nFlowTracks][8]  = track->firstPoint.z();
	
	if((track->charge()) > 0){
	histogram[6] -> Fill( (float) track->pt(),1./eff_pt);	  
	histogram2D[3]->Fill(1*trackP,1/trackBeta);
	}else if((track->charge()) < 0){
	histogram[7] -> Fill( (float) track->pt(),1./eff_pt);	  
	histogram2D[3]->Fill(-1*trackP,1/trackBeta);
	}
	//==========================
	}else continue;
	}*/
      //==========================
      //      ran_map[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][nFlowTracks] = nFlowTracks;
      //nFlowTracks++;
    }//end of while loop, must enclose nflow++ and ran_map[][][]
  //MAIN CALCULATION!!!!!
    
  if(EP->CalcEventPlane(CentralityID, tmp_event.Event_ZVert_Class) ){
      int RpID =EP->GetRpID(EP->psi2(1));
      
      if(RpID<0) return kStOK;
      tmp_event.rplane= EP->psi2(0);
      tmp_event.Event_Rp_Class=RpID;
      
      int qbinID = (EP->GetQBin(1,1));
      if(qbinID<=-999) return kStOK;    
      
      cls_id.qbin_class = qbinID;
      cls_id.rp_class=cls_id.qbin_class;
      hbt->CalcRealEvent (&tmp_event, &cls_id );
      hbt->CalcMixedEvent( &tmp_event, &cls_id );
      hbt->SetNumberPairsClassified(CentralityID);
      //tmp_event.sgl_trks[0].clear();
      //tmp_event.sgl_trks[1].clear();
  }
  EP->Reset();
  //===========================

  /*
  nFlowTracks1[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][0] = nFlowTracks  ;
  //===========================
  for(int ii = 0; ii<(nFlowTracks - 1); ii++)
    {
      int rr = ii + (int)(rand->Rndm() * (nFlowTracks-ii));
      int temp = ran_map[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][ii];
      ran_map[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][ii] = ran_map[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][rr]; 
      ran_map[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][rr] = temp;
    }
  //=========================== 
  //=========================== 
  //======Particle1============ 
  //===========================
  Ntrack   =  nFlowTracks1[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][0];
  //===========================
  for( int i1=0; i1 < Ntrack ; i1++ )
    {
      int random_index = ran_map[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][i1];
      trackpx         = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][random_index][0];
      trackpy         = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][random_index][1];
      trackpz         = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][random_index][2];
      trackE          = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][random_index][3];
      trackW          = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][random_index][4];
      trackEta        = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][random_index][5];
      trackPt         = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][random_index][6];
      //      trackZ          = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][random_index][7];


      //=========================== 
      //======Particle2_same======= 
      //=========================== 
      for( int i2 = i1+1; i2 < Ntrack ; i2++ )
	{
	  int asso_same_index = ran_map[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][i2];
	  if(asso_same_index == random_index) continue;
	  asso_same_px      = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][asso_same_index][0];
	  asso_same_py      = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][asso_same_index][1];
	  asso_same_pz      = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][asso_same_index][2];
	  asso_same_E       = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][asso_same_index][3];
	  asso_same_W       = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][asso_same_index][4];
	  asso_same_Eta     = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][asso_same_index][5];
	  asso_same_Pt      = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][asso_same_index][6];
	  //          asso_same_Z       = DataArray[mEventsProcessed[CentralityID][trackzx]%10][CentralityID][trackzx][asso_same_index][8];


	  //================================CALCULATIONS FOR Q COORDS=========================
	   
	  float ave_mom_x = 0.5 * ( trackpx + asso_same_px);
	  float ave_mom_y = 0.5 * ( trackpy + asso_same_py );
	  float ave_mom_t = sqrt( ave_mom_x * ave_mom_x + ave_mom_y * ave_mom_y );
	  float d_mom_x  = trackpx - asso_same_px;
	  float d_mom_y  = trackpy - asso_same_py;
	  float d_mom_z  = trackpz - asso_same_pz;
	  float d_energy = trackE-asso_same_E;
	  float Beta_z  = ( trackpz + asso_same_pz )/(trackE+asso_same_E );
	  float Gamma_z = 1.0 / sqrt( 1.0 - Beta_z * Beta_z );
	  //LCMS frame                                                                                                                                                           
	  float Q_inv  = sqrt( fabs( pow(d_mom_x,2.0) + pow(d_mom_y,2.0) + pow(d_mom_z,2.0) - pow(d_energy,2.0) ) );                                                           
	  float Q_long = Gamma_z * ( d_mom_z - Beta_z * d_energy );
	  float Q_out  = ( ave_mom_x * d_mom_x + ave_mom_y * d_mom_y ) / ave_mom_t;
	  float Q_side = ( ave_mom_x * d_mom_y - ave_mom_y * d_mom_x ) / ave_mom_t;
	  //==========================================================	 
	  double kt = 0.5 *fabs(asso_same_Pt+trackPt);
	  int KtID;
	  //	  int AveMomID;
	  if(.15<kt && kt<.25) KtID =1;
	  else if(.25<kt && kt<.35) KtID =2;
	  else if(.35<kt && kt<.45) KtID =3;
	  else if(.45<kt && kt<.6) KtID =4;
	  else continue; //KtID= -999;
	  //-----------------------------ave mom id kt
	  if(CentralityID==9){
	  histogram[19]->Fill(ave_mom_t,trackW*asso_same_W);
          histogram[18]->Fill(kt,trackW*asso_same_W);
	  }if(CentralityID==8){
	    histogram[21]->Fill(ave_mom_t,trackW*asso_same_W);
	    histogram[20]->Fill(kt,trackW*asso_same_W);
	  }if(CentralityID==7){
	    histogram[23]->Fill(ave_mom_t,trackW*asso_same_W);
	    histogram[22]->Fill(kt,trackW*asso_same_W);
	  }
	  double PairRapidity = .5*log( (trackE+asso_same_E+trackpz+asso_same_pz)/(trackE+asso_same_E-trackpz-asso_same_pz) );
	  double trackrap = 0.5 * log( (trackE+trackpz )/(trackE-trackpz));
	  double asso_same_rap = 0.5 * log( (asso_same_E+asso_same_pz )/(asso_same_E-asso_same_pz));
	  histogram[14]->Fill(trackrap);
	  histogram[15]->Fill(trackrap-asso_same_rap);
	  histogram[16]->Fill(asso_same_Eta-trackEta);
	  histogram[17]->Fill(PairRapidity);
	  histogram[12]->Fill(kt);
	  //if(fabs(trackrap-asso_same_rap)<0.5) histogram2D[4]->Fill(fabs(trackPhi-asso_same_Phi),fabs(trackZ-asso_same_Z),trackW*asso_same_W);
	  if(fabs(asso_same_Eta-trackEta)>0.04 && fabs(trackrap-asso_same_rap)<0.5 ){
	    histogram[13]->Fill(ave_mom_t);
	    histogram[8]->Fill(ave_mom_t,trackW*asso_same_W);
	    histogram[9]->Fill(kt,trackW*asso_same_W);
	    same_event[0][KtID][CentralityID]->Fill(Q_out,Q_side,Q_long,0.5*(trackW*asso_same_W));
	    same_event[0][KtID][CentralityID]->Fill(-Q_out,-Q_side,-Q_long,0.5*(trackW*asso_same_W));	    
	    same_qinv[0][KtID][CentralityID]->Fill(Q_inv, 0.5*(trackW*asso_same_W));
	    same_qinv[0][KtID][CentralityID]->Fill(-Q_inv,0.5*(trackW*asso_same_W));

	    //qinv_arrays
	    HArray_qinv[0][KtID][CentralityID]->Fill(      Q_out, Q_side, Q_long   ,Q_inv);
	    HArray_qinv[0][KtID][CentralityID]->Fill(     -Q_out,-Q_side,-Q_long   ,Q_inv);
	    //containing number of entries for averages
	    HArray_qinv_entr[0][KtID][CentralityID]->Fill( Q_out, Q_side, Q_long);
	    HArray_qinv_entr[0][KtID][CentralityID]->Fill(-Q_out,-Q_side,-Q_long);

	  }else continue;//rap&&eta
	  //&&&&&&&&&&&&
	}//particle2loop
      //&&&&&&&&&&&&&&&
      //===========================
      //======Particle2_mex======== 
      //=========================== 
      int maxk = (mEventsProcessed[CentralityID][trackzx] < 10) ? mEventsProcessed[CentralityID][trackzx] : 10 ;
      
      for(int k = 0; k < maxk; k++)
	{
	  if( mEventsProcessed[CentralityID][trackzx]%10 == k ) continue;
	  
	  Nasso = nFlowTracks1[k][CentralityID][trackzx][0];
	  
	  for(int k1 = 0; k1 <Nasso ; k1 ++)
	    {
	      int asso_index = ran_map[k][CentralityID][trackzx][k1];
	      //============================
	      assopx        = DataArray[k][CentralityID][trackzx][asso_index][0];
	      assopy        = DataArray[k][CentralityID][trackzx][asso_index][1];
	      assopz        = DataArray[k][CentralityID][trackzx][asso_index][2];
	      assoE         = DataArray[k][CentralityID][trackzx][asso_index][3];  
	      assoW         = DataArray[k][CentralityID][trackzx][asso_index][4];
	      assoEta       = DataArray[k][CentralityID][trackzx][asso_index][5];
	      assoPt        = DataArray[k][CentralityID][trackzx][asso_index][6];
	      //	      assoPhi        = DataArray[k][CentralityID][trackzx][asso_index][7];
	      //              assoZ        = DataArray[k][CentralityID][trackzx][asso_index][8];

	      //============================

	      float assoave_mom_x = 0.5 * ( trackpx + assopx);
	      float assoave_mom_y = 0.5 * ( trackpy + assopy );
	      float assoave_mom_t = sqrt( assoave_mom_x * assoave_mom_x + assoave_mom_y * assoave_mom_y );
	      float assod_mom_x  = trackpx - assopx;
	      float assod_mom_y  = trackpy - assopy;
	      float assod_mom_z  = trackpz - assopz;
	      float assod_energy = trackE-assoE;
	      //calc pair velocity Beta                                                                                                                                           
	      float assoBeta_z  = ( trackpz + assopz ) / (trackE+assoE );
	      //calc gamma factor along z-direction                                                                                                                               
	      float assoGamma_z = 1.0 / sqrt( 1.0 - assoBeta_z * assoBeta_z );
	      //LCMS frame                                                                                                                                                        
	      float Q_inv  = sqrt( fabs( pow(assod_mom_x,2.0) + pow(assod_mom_y,2.0) + pow(assod_mom_z,2.0) - pow(assod_energy,2.0) ) );                                        
	      float asso_Q_long = assoGamma_z * ( assod_mom_z - assoBeta_z * assod_energy );
	      float asso_Q_out  = ( assoave_mom_x * assod_mom_x + assoave_mom_y * assod_mom_y ) /assoave_mom_t;
	      float asso_Q_side = ( assoave_mom_x * assod_mom_y - assoave_mom_y * assod_mom_x ) / assoave_mom_t;
	      //=======================================================================================================================
	      double assoKt = 0.5*(assoPt+trackPt);
	      int assoKtID;
	      //	      int assoAveMomID;
	      if(.15<assoKt && assoKt<.25) assoKtID =1;
	      else if(.25<assoKt && assoKt<.35) assoKtID =2;
	      else if(.35<assoKt && assoKt<.45) assoKtID =3;
	      else if(.45<assoKt && assoKt<.6) assoKtID =4;
	      else continue;	      //-----------------------------ave mom id kt
	      //   	      if(.15<assoave_mom_t && assoave_mom_t<.25) assoAveMomID = 5;
	      //else if(.25<assoave_mom_t && assoave_mom_t<.35) assoAveMomID = 6;	      
	      //else if(.35<assoave_mom_t && assoave_mom_t<.45) assoAveMomID = 7;
	      //else if(.45<assoave_mom_t && assoave_mom_t<.6) assoAveMomID = 8;
	      //else continue;
	      //=============================================================================================
	      //	      int Biny=floor( (.195+asso_Q_side)/.01 )+1;
	      //int Binz=floor( (.195+asso_Q_long)/.01 )+1;
	      //===============================================================================================
	      double trackRapidity = 0.5 * log( (trackE+trackpz)/(trackE-trackpz) );
	      double mixRapidity   = 0.5 * log( (assoE+assopz)/(assoE-assopz) );
	      //	      if(fabs(trackRapidity-mixRapidity)<0.5) histogram2D[5]->Fill(fabs(trackPhi-assoPhi),fabs(trackZ-assoZ),trackW*assoW);
	      //      float PairRapidity  = .5*log( (trackE+assoE+trackpz+assopz)/(trackE+assoE-trackpz-assopz) );
	      //=================================================================================================
	      if( fabs(assoEta-trackEta)> 0.04 && fabs(trackRapidity-mixRapidity)<0.5) {
		histogram[10]->Fill(assoave_mom_t,trackW*assoW);
		histogram[11]->Fill(assoKt,trackW*assoW);
		//=========================================kt====================================================
		  mix_event[0][assoKtID][CentralityID]->Fill(asso_Q_out,asso_Q_side,asso_Q_long,0.5*(trackW*assoW));
                  mix_event[0][assoKtID][CentralityID]->Fill(-asso_Q_out,-asso_Q_side,-asso_Q_long,0.5*(trackW*assoW));
		  
		  mix_qinv[0][assoKtID][CentralityID]->Fill(Q_inv,0.5*(trackW*assoW));
                  mix_qinv[0][assoKtID][CentralityID]->Fill(-Q_inv,0.5*(trackW*assoW));

	      }else continue;//rap&&eta
	      //============================
	      //============================	      
	    }//mixloop
	}//particle2loop
    }//particle1loop
  */
    mEventsProcessed1++;
    //**********
    //  mEventsProcessed[CentralityID][trackzx]++;
  
  return kStOK ;
}
//=====================Finish===========================
Int_t MyAnalysisMaker::Finish( )
{                                                              
  cout<<"about to write to file..."<<endl;
  histogram_output -> Write() ;   // Write all histograms to disk if ( mEventsProcessed1 >= MinEvents ) 
  if( !hbt->End() ) cout<<" HBTmixcheck::End() Sucessfull ;)"<<endl;
  histogram_output -> Write();  // Write all histograms to disk
  histogram_output -> Close() ;   // close all histograms to disk
  //delete EP;
  delete histogram_output;
  return kStOK ;
}
//====================accepttrigger======================
bool MyAnalysisMaker::accepttrigger(StMuEvent* muEvent)
{
  if ( muEvent->triggerIdCollection().nominal().isTrigger(280001) ||
       muEvent->triggerIdCollection().nominal().isTrigger(280002) ){
    return true ;
  }else{
    cout<<"fail trig"<<endl;
    return false ;
  }
  
}
//==================RejectRunNumbers======================
bool MyAnalysisMaker::RejectRunNumbers(StMuEvent* muEvent)
{
  
  const Int_t bad_run_list_39GeV[38]  = {11199124,11100002,11100045,11101046,11102012,11102051,11102052,11102053,11102054,11102055,11102058,11103035,11103056,11103058,11103092,11103093,11105052,11105053,11105054,11105055,11107007,11107042,11107057,11107061,11107065,11107074,11108101,11109013,11109077,11109088,11109090,11109127,11110013,11110034,11110073,11110076,11111084,11111085};
  
   Int_t runno=muEvent->runNumber();
   for(Int_t j=0; j<38; j++)
     {
       if (runno==bad_run_list_39GeV[j]) return false;
     }
   
   return true;
}
//====================acceptevent======================
bool MyAnalysisMaker::acceptevent(StMuEvent* muEvent)
{  
  // Cut parameters for each event
  //===========================================
  const Float_t VertexXMin  =  -1.0 ; //cm 
  const Float_t VertexXMax  =   1.0 ; // cm
  const Float_t VertexYMin  =  -1.0 ; // cm 
  const Float_t VertexYMax  =   1.0 ; // cm
  const Float_t VertexZMin  = -40.0 ; // cm       
  const Float_t VertexZMax  =  40.0 ; // cm   
  const Int_t   MultMin     =    0  ; // Note: this is a cut on refMult which is not your ordinary multiplicity.
  const Int_t   MultMax     = 10000 ; // Refmult corresponds to Zhangbu's ascii table of cuts for centrality bins.  
  const Int_t   BlackEvent  = 15000 ; // Maximum number of primary tracks allowed in one event (more than this is an error)
  //=============================================
  Int_t Multiplicity =  muEvent->refMult() ;  // Reference Multiplicity for Centrality determination
  Int_t NumberOfPrimaryTracks   =  mMuDstMaker->muDst()->numberOfPrimaryTracks()   ;
  if ( Multiplicity < MultMin     || Multiplicity > MultMax )              return false ;
  if ( NumberOfPrimaryTracks <= 0 || NumberOfPrimaryTracks > BlackEvent )  return false ;
  //==============================================
  // Cut on the number of vertices in the event.  On old tapes, no-vertex gets reported as VtxPosition=(0,0,0).
  // On newer tapes, we have a value for the number of vertices ... but this value is reported equal to zero on
  // old tapes that have a good vertex ... so we use both methods of choosing events with only one vertex.                     
  //==============================================
  Int_t   NumberOfPrimaryVertices =  mMuDstMaker->muDst()->numberOfPrimaryVertices() ;
  if (NumberOfPrimaryVertices == 0 ) return false  ;                         // Skip events with > 1 primary vertex
  if (NumberOfPrimaryVertices >= 3 ) return false  ;                         // Skip events with > 1 primary vertex  
  //===============================================
  Float_t vertex[3] = {0} ;
  vertex[0] = muEvent->primaryVertexPosition().x() ;
  vertex[1] = muEvent->primaryVertexPosition().y() ;
  vertex[2] = muEvent->primaryVertexPosition().z() ;
  //==============================================
   if (TMath::Abs(muEvent->primaryVertexPosition().x()) < 1.e-5 &&
       TMath::Abs(muEvent->primaryVertexPosition().y()) < 1.e-5 &&
       TMath::Abs(muEvent->primaryVertexPosition().z()) < 1.e-5){
     return false ;  // Skip events without a primary vertex
      }
  //==============================================
 // Cut on Vertex location 
  if ( vertex[0] < VertexXMin || vertex[0] > VertexXMax )    return false ;  // Skip events that fall outside Vtx cuts 
  if ( vertex[1] < VertexYMin || vertex[1] > VertexYMax )    return false ;
  if ( vertex[2] < VertexZMin || vertex[2] > VertexZMax )    return false ;

  return true ;
  
}
//============================accepttrack=======================                                                                                                                                                   
bool MyAnalysisMaker::accepttrack(StMuTrack* track){
  // Cut Parameters for individual tracks                                                                                                                                                                          
  if ( track->flag() == 0 ) return false ; // Skip bad tracks.  'flag()' tells if its bad, a primary, a global, etc.                                                                                               
  const Float_t dcaCut   =   3.0  ; // cm
  const Float_t PtMin    =   0.2  ; // GeV                                                                                                                                                                        
  const Float_t PtMax    =   4.0  ; // GeV    //For pt dependence                                                                                                                                                  
  const Float_t EtaMin   =  -1.0  ;
  const Float_t EtaMax   =   1.0  ;
  const Float_t FitRatio =  0.52  ;
  const Int_t   nHitPossMin =  5  ;
  if ( track->flag() <  0  )     return false ;          // Track quality                                                                                                                                          
  if ( track->dcaGlobal().mag() > dcaCut  )  return false ;   // 3D DCA for global tracks                                                                                                                          
  if ( track->nHitsPoss() <  nHitPossMin )  return false ;    // Minimum number of Possible hits, see above.                                                                                                       
  if ( track->eta()       <  EtaMin  || track->eta()      >  EtaMax  ) return false ;
  if ( track->pt() <  PtMin  || track->pt()  >  PtMax   ) return false ;
  if ( ( (float)track->nHitsFit() / (float)track->nHitsPoss() ) < FitRatio )  return false ;
  //===========TPC=============                                                                                                                                                                                    
  if( track->eta() > -1 && track->eta()< 1  )
    {
      const Int_t   nHitMax  =   100  ;
      const Int_t   nHitMin  =    15  ;      // 5 is FTPC                                                                                                                                                          
      //**********                                                                                                                                                                                                 
      if ( track->nHitsFit()  <  nHitMin || track->nHitsFit() >  nHitMax ) return false ;
    }
  //==============FTPC==========                                                                                                                                                                                   
  else if(fabs(track->eta()) > 2.5){
    const Int_t   nHitMax  =   100  ;
    const Int_t   nHitMin  =    5  ;      // 5 is FTPC                                                                                                                                                             
    if ( track->nHitsFit()  <  nHitMin || track->nHitsFit() >  nHitMax ) return false ;
  }else{
    return false ;
  }
  //***************************                                                                                                                                                                                    
  return true ;
}
//=========================centrality===========================
Int_t MyAnalysisMaker::centrality( Int_t referenceMultiplicity )
{
  Int_t   MiddleBinID     [] = {  0 ,  1 ,  2 ,  3 ,   4 ,   5 ,   6 ,   7 ,  8 ,    9 } ;  // ID Number   
  Int_t   myCentrality  ;
  //=============================
  if      ( referenceMultiplicity ==-1 ) { myCentrality = MiddleBinID[0] ;}
  else if ( referenceMultiplicity == 0 ) { myCentrality = MiddleBinID[1] ;}
  else if ( referenceMultiplicity == 1 ) { myCentrality = MiddleBinID[2] ;}
  else if ( referenceMultiplicity == 2 ) { myCentrality = MiddleBinID[3] ;}
  else if ( referenceMultiplicity == 3 ) { myCentrality = MiddleBinID[4] ;}
  else if ( referenceMultiplicity == 4 ) { myCentrality = MiddleBinID[5] ;}
  else if ( referenceMultiplicity == 5 ) { myCentrality = MiddleBinID[6] ;}
  else if ( referenceMultiplicity == 6 ) { myCentrality = MiddleBinID[7] ;}
  else if ( referenceMultiplicity == 7 ) { myCentrality = MiddleBinID[8] ;}
  else                                   { myCentrality = MiddleBinID[9] ;}
  //=============================
  return myCentrality ;  
}
//................................
//--------------------------------------------------------
bool MyAnalysisMaker::ProtonCheck(Float_t proton,Float_t beta, Float_t p){

  if (proton <-2.0 || proton > 2.0) return false;

  if(beta == -999) return false;  

  float masssqr = p*p*(1.0/(beta*beta)-1.0);//mass squre
  
 if (masssqr <0.8 || masssqr > 1.0) return false ;

   return true ;

 }
//------------------------------------------------------
bool MyAnalysisMaker::PionCheck(Float_t pion,Float_t beta, Float_t p){

 if (pion <-2.0 || pion > 2.0) return false;

 if(beta == -999) return false;

 float masssqr = p*p*(1.0/(beta*beta)-1.0);//mass squre

 if (masssqr < -0.02 || masssqr > 0.10) return false ;

  return true;

 }
//-----------------------------------------------------------
bool MyAnalysisMaker::KaonCheck(Float_t kaon,Float_t beta, Float_t p){

 if (kaon <-2.0 || kaon > 2.0) return false;

 if(beta == -999) return false;

   float masssqr = p*p*(1.0/(beta*beta)-1.0);//mass squre

   if (masssqr <0.20 || masssqr > 0.35) return false ;

  return true;

 }
