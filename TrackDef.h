#ifndef __TRACK_DEF_H__
#define __TRACK_DEF_H__

#include <vector>

//single track
struct hbt_sgl_trk {

  //track information
  float mom_x;
  float mom_y;
  float mom_z;
  float Energy;
  float theta;
  float eta;
  float dc_zed;
  float dc_phi;
  float pc1_x;
  float pc1_y;
  float pc1_z;
  float pc3_x;
  float pc3_y;
  float pc3_z;
  float emtof_x;
  float emtof_y;
  float emtof_z;
  float ptof_x;
  float ptof_y;
  float ptof_z;
  float emc_z;  //temp
  float emc_phi;//temp
  int tower_id;
  int particle_id;
  int detector_id;
};
typedef std::vector<hbt_sgl_trk> Trks;
typedef std::vector<Trks> TrksBuff;
 
//event info + single track
struct event_trk {

  //event information
  float centrality;
  float zvertex;
  float rplane;
  //track information
  Trks sgl_trks[2];
  //Added by Ben
  int Event_Cent_Class;
  int Event_ZVert_Class;
  int Event_Rp_Class;
};

typedef struct relative_momentum {
	float q_inv;
	float q_side;
	float q_out;
	float q_long;
	float phi_pair;
	float kt;
	int kt_class;
} RelativeMom;

typedef struct class_id {
  int cnt_class;
  int rp_class;
  int qbin_class;
} ClassID;

#endif /* __TRACK_DEF_H__ */
