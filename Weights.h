#pragma once 
#include "TH2.h"
#include "TH1.h"
#include "TMath.h"
class Weights
{
public:
  
 Weights(int cent,int zcut):Cent_hbt(cent),Zcut(zcut),pi(TMath::Pi()){};
  //Weights(int a, int b):Cent_hbt(a),Zcut(b){};
  ~Weights(){};
  void CreateWeightHistos(int mySagita,float eta,float phi,int CentralityID,int trackzx,bool corrected=false);
  Float_t getWeight(float _pt, float _eta, float _phi, int _mysagita, int _mycentral, int _vzz);
  bool IsWeightsGood;
  TH2F* Etaphi[10][10][50] ;
  void initEtaPhi(bool);
  void SetWeightsGood();

 private:
  TH2F* EtaphiW[10][10][50] ;
  TH1F* Etah[10];
  TH1F* Phih[10];
  TH1F* PTh[10];
  const int Cent_hbt;
  const int Zcut;
  TH1F* histogram[20];
  const float pi ;
  int Sagita(float pt, int charge);
  
  
};
