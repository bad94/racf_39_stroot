#ifndef __COULOMBWAVE_HH__
#define __COULOMBWAVE_HH__

//////////////////////////////////////////////////////////////////////////////////////
//
// CoulombWave class : Description
//
//  This class calculates the full coulomb correction factor, originally developed by
//  some NA44's collaborators in FORTARAN.
//  This function calculates the coulomb wave function and the probability of some
//  random points according to the gaussian source radii and q values.
//  The number of random points is set to 20 initially, but you can change it by using
//  Set_Gampt function. As the number increase, the calculation become accurate. 
//  But it will take for long time to calculate.
//
//  The factor is obtained by Coul_Wave(Mass, Rinv, Rts, Rto, Rl, Qinv, Qts, Qto, Ql).
//  Mass               : Particle ideal mass.
//  Rinv, Rts, Rto, Rl : 1-d source radius, and ource radii in side, out, long directions.
//  Qinv, Qts, Qto, Ql : q invariant and q_side,out,long values.
//  *note: this function is revised as include Rinv. (01/26/03 A.E.)
//
//  This factor is assumed to be applied to actual pairs, so if you apply this factor 
//  to background pairs, the factor should be 1/Coul_Wave().
//
//  Written by Akitomo Enokizono (enoki@bnl.gov) 06/14/02
//
//////////////////////////////////////////////////////////////////////////////////////

//#include <fstream>
#include <iostream>
#include <TRandom.h>
#include <TMath.h>
#include <complex>
#include <math.h>

typedef std::complex<float> cmplex;
class CoulombWave {
private:
  Int_t gam_pt;
  Int_t qtype;

public:

  CoulombWave();
  ~CoulombWave();

  void Set_Gampt(Int_t nopt) { gam_pt=nopt; std::cout<<"GAM_PT = "<<gam_pt<<std::endl; }
  void Set_Qtype(Int_t qtyp) { qtype=qtyp; std::cout<<"Q Type = "<<qtype<<std::endl; }
  float Coul_Gamow(Float_t Mass, Float_t Qinv);
  float Coul_Wave(const Float_t &Mass,const Float_t &Rinv_i,const Float_t &Rts_i,const Float_t &Rto_i,const Float_t &Rl_i, 
		  const Float_t &Qinv, const Float_t &Qts, const Float_t &Qto, const Float_t & Ql);

  cmplex CLog(const cmplex &a_in);
  cmplex Conjg(const cmplex &a_in);
  cmplex CPower(const cmplex &Xin, const cmplex &Yin);
  cmplex CGamma(const cmplex &a_in);
  cmplex CHyper(const cmplex & Ain,const cmplex & Bin,const cmplex & Zin,const cmplex & f1in,const cmplex & f2in,const cmplex (& hpin)[20],const cmplex (& hp1in)[4],const  cmplex (& hp2in)[4] );


};

#endif /*__COULOMBWAVE_HH__*/
