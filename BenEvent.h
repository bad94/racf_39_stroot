#ifndef __BEN_EVENT_H__
#define __BEN_EVENT_H__
#include <vector>

class BenTrack
{
 public:
  BenTrack(float initval=0){P=initval;pt=initval;eta=initval;phi=initval;beta=initval;DCA=initval;
  DeDx=initval; nSigmaPion=initval; nSigmaKaon=initval; nSigmaProton=initval; nSigmaElectron=initval;
  }
  virtual ~BenTrack(){}
  float P  ;
  float pt;
  float eta;
  float phi;
  float beta;
  float DCA;
  float DeDx;
  float nSigmaPion;
  float nSigmaKaon;
  float nSigmaProton;
  float nSigmaElectron;
};

class BenEvent
{
 public:
  
  BenEvent(){}
  virtual ~BenEvent(){TrackVector.clear();}
  float Multip; 
  float Vz;
  float Vx;
  float Vy;
  float vpdVz;
  float Nvertex;
  std::vector<BenTrack> TrackVector; 
};
#endif /* __BEN_EVENT_H__ */
