// Based on the MuDST tools written by Frank Laue.
// Based on the DST Tutorial by Dan Magestro on the STAR Computing/Tutorials page.
// Updated 9/4/2006 by Jim Thomas to include the latest DST format, and Scheduler techniques.

#ifndef MyAnalysisMaker_def
#define MyAnalysisMaker_def
#include <iostream>
#include "StMaker.h"
#include "TString.h"
#include "StRoot/StRefMultCorr/StRefMultCorr.h"
//#include "HBTmixcheck.h"
//#include "TrackDef.h"

class StMuDstMaker  ;
class StMuEvent     ;
class StMuTrack     ;
class TFile         ;
class TH1F          ;
class TH2F          ;
class TH3F          ;
class TH2D          ;
class TProfile2D    ;
class TProfile3D    ;
class TRandom       ;
class TRandom3      ;
class StRefMultCorr ;
class HBTmixcheck   ;
class EventPlane    ;
#define MaxNumberOfTH1F      100
#define MaxNumberOfTH2F      100
#define MaxNumberOfTH3F      150
#define MaxNumberOfTH2D      200
#define MaxNumberOfTH3D      100
#define MaxNumberOfTProfile  200


class MyAnalysisMaker : public StMaker
{  
 private:
  HBTmixcheck* hbt;  
  EventPlane* EP;
  StMuDstMaker* mMuDstMaker ;
  int Cent_hbt;
  int Zcut;
  //==========================================
  TH1F*         histogram[MaxNumberOfTH1F]   ;
  TH2F*         histogram2D[MaxNumberOfTH2F] ;    
  //==========================================
  //==========================================
  /*
  TH3F*   same_event[1][10][10];
  TH3F*   mix_event[1][10][10] ;

  TH3F*   HArray_qinv[1][10][10];
  TH3F*   HArray_qinv_entr[1][10][10];

  TH1F*   same_qinv[1][10][10];
  TH1F*   mix_qinv[1][10][10];
  */
  //==========================================
  //=========================================
  TFile*        histogram_output                      ;     //  Histograms outputfile pointer
  TRandom*      rand                                  ;     //  Random number generator
  StRefMultCorr *refmultCorrUtil                      ;
  ULong_t       mEventsStarted                        ;     //  Number of Events read
  ULong_t       mEventsProcessed1                     ;
  ULong_t       mEventsProcessed[10][20]              ;     //  Number of Events processed and analyzed
  TString       mHistogramOutputFileName              ;     
  bool          accepttrack   (StMuTrack*)            ;     //  Function to make cuts on track quality (i.e. nhits or eta)     
  bool          acceptevent   (StMuEvent*)            ;     //  Function to make cuts on event quanitites (ie. vertex position)
  bool          accepttrigger (StMuEvent*)            ;     //  Function to make cuts on the trigger words (Cusomize each year)
  Int_t         particleID    (StMuTrack*)            ;     //  Function to define the ID of a particle  
  Int_t         centrality ( Int_t refMult )          ;     //  Function to calculate c
  bool          RejectRunNumbers(StMuEvent*)          ;
  bool   ProtonCheck(Float_t nsigma,Float_t beta,Float_t p);
  bool   PionCheck(Float_t nsigma,Float_t beta,Float_t p);
  bool   KaonCheck(Float_t nsigma,Float_t beta,Float_t p);

 public:
  
  MyAnalysisMaker(StMuDstMaker* maker)   ;          //  Constructor
  virtual          ~MyAnalysisMaker( )   ;          //  Destructor
  
  Int_t Init    ( ) ; //  Initiliaze the analysis tools ... done once
  Int_t Make    ( ) ; //  The main analysis that is done on each event
  //  Int_t Clear   ( ) ;
  Int_t Finish  ( ) ;  //  Finish the analysis, close files, and clean up.
  
  void SetOutputFileName(TString name) { mHistogramOutputFileName = name ; } // Make name available to member functions
  
  ClassDef(MyAnalysisMaker,1)  //  Macro for CINT compatability
    
    };

#endif


